/**
 * Created by Rainc on 15-3-25.
 */

var mysql = require('./controllers/basedao.js');
var log4js = require('log4js');
var config = require('./config.json')
var Push = require('./controllers/api.js');

log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.logFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'BAIDUPUSH'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('BAIDUPUSH');
logger.setLevel('INFO');

//向客户端推送消息
function pushMsg(client,type,param) {
  var opt = {};
  if(type == 'alarm'){
    opt = {
      push_type: 2,
      tag: 'grp_'+ param.domain_uuid + '_alm',
      messages: JSON.stringify({
        t: 'a',
        h: '告警通知',
        l: param.alarm_level,
        m: param.report_time,
        i: param.alarm_id,
        n: param.alarm_name,
        o: param.object_brief,
        c: param.alarm_desc_cn + '/' + param.alarm_desc
      }),
      msg_keys: JSON.stringify("alarmKey")
    }
  }
  client.pushMsg(opt, function(err, result) {
    if (err) {
      logger.error(err);
      return;
    }else{
      var sql = "UPDATE tbl_alarm set send_flag=1 WHERE uuid = " + param.uuid
      mysql.Cb_process(sql,null,function(err){
        if(err){
          logger.error(err);
          logger.error('更新tbl_alarm告警uuid：' + param.uuid + '的发送标识失败！')
        }
      })
    }
  })
}

function getUntreatedAlarm(){
  //查询未处理的告警
  var sql = "SELECT a.uuid,a.alarm_id,b.alarm_level,b.alarm_name,a.object_brief,domain_uuid,alarm_desc,alarm_desc_cn,a.report_time from tbl_alarm a LEFT JOIN tbl_alarm_desc b on a.alarm_id = b.alarm_id WHERE a.confirm_flag = 0 and a.send_flag = 0 and b.alarm_level < 5";
  mysql.find(sql,function(err,results){
    if(err){
      logger.error("查询未处理告警出错！ " + err)
    }else if(!results){

    }else{
//    console.log(results);
      var opt = {
        ak: 'o7h7CZXpwLC0phUtWGsOMcSK',
        sk: 'NmdVBTj8S8zrhnrQ1LGdvkTowsg48NVZ'
      };
      var client = new Push(opt);
      for(var i in results){
        results[i].alarm_level = results[i].alarm_level == 6 ? 'I' : (results[i].alarm_level == 5 ? 'N' : (results[i].alarm_level == 4 ? 'W' : (results[i].alarm_level == 2 ? 'C' : (results[i].alarm_level == 0 ? 'E' : ''))))
        pushMsg(client,'alarm',results[i])
      }
    }
  })
}

//设置多长时间进行查询发送告警消息
setInterval(function(){
  getUntreatedAlarm()
},60000);
